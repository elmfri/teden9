# Naloge v prvem tednu Elm@FRI

Repozitorij si najprej klonirajte na svoj računalnik z

```
git clone https://bitbucket.org/elmfri/teden9
```
V direktoriju `src` imate datoteko `Main.elm`, v katero boste pisali vaše rešitve.

Pripravite si strukturo za testiranje (tj. zaženite elm-test init).


## Naloga 1
Napišite funkcijo

```elm
reverse : List a -> List a
```
ki obrne podani seznam, npr. seznam [1,2,3] postane [3,2,1].

Naslednja funkcija je
```elm
rotateLeft : List a -> List a
```
ki seznam zarotira v desno, npr. seznam [1,2,3,4] postane [2,3,4,1].

Še ena klasika:
```elm
isPalindrome : List Int -> Bool
```
ki preveri, če je podani seznam celih števil palindrom.

In zadnja funkcija naj bo
```elm
sort : List Int -> List Int
```
ki podani seznam celih števil uredi naraščajoče. Za urejanje uporabite poljuben algoritem

## Naloga 2.

Vse zgoraj podane funkcije pretestirajte. Za vsako napišite nekaj unit testov, nato pa še dovolj fuzzy testov, da napačna implementacija ne bi mogla prestati testiranja teh lastnosti.

V testiranje vključite tudi vse morebitne pomožne funkcije, ki jih boste morali napisati.
